function nthPrime(n) {
  let count = 0;
  let i = 1;
  while (count < n) {
    let isPrime = true;
    i++;
    for (let j = 2; j <= Math.sqrt(i); j++) {
      if (i % j == 0) {
        isPrime = false;
        break;
      }
    }
    if (isPrime) {
      count++;
    }
  }
  return i;
}

console.log(nthPrime(6));
