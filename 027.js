// Prime generator
let primesInOrder = [2]

function updatePrimes() {
  let n = primesInOrder.length;
  for (let i = 0; i < n; i++) {
    let next = primesInOrder[n + i - 1];
    let isPrime = false
    while (!isPrime) {
      next++;
      isPrime = true;
      for (let j = 2; j <= Math.sqrt(next); j++) {
        if (next % j === 0) {
          isPrime = false;
          break;
        }
      }
    }
    primesInOrder.push(next);
  }
}
// Primes in order (i.e. 2,3,5,7,11,...)
function primes(a, b) {
  // Primes generated
  let n = 0;
  while (true) {
    let possiblePrime = n*n + a*n + b;
    // Generate primes if needed
    while (primesInOrder.length <= n) {
      updatePrimes();
    }
    if (possiblePrime !== primesInOrder[n]) {
      break;
    }
    n++;
  }
  return n;
}

// Primes in arbitrary order
function primes(a, b) {
  // Primes generated
  let n = 0;
  while (true) {
    let possiblePrime = n*n + a*n + b;
    if (possiblePrime < 2) {
      break;
    }
    let isPrime = true;
    for (let j = 2; j <= Math.sqrt(possiblePrime); j++) {
      if (possiblePrime % j === 0) {
        isPrime = false;
        break;
      }
    }
    if (isPrime) {
      n++;
    }
    else {
      break;
    }
  }
  return n;
}

function quadraticPrimes(range) {
  // Store product that makes the most amount of primes
  // and that amount
  let product = 0;
  let n = 0;
  // For every duo in range
  for (let a = -range + 1; a < range; a++) {
    for (let b = -range; b <= range; b++) {
      // Count primes generated
      let m = primes(a, b);
      if (m > n) {
        // Stay with the biggest amount and store product
        n = m;
        product = a * b;
      } 
    }
  }
  return product;
}

quadraticPrimes(200);