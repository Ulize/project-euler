function sumFactorialDigits(n) {
  return BigInt(factorial(n)).toString().split('').reduce((sum, digit) => sum + parseInt(digit), 0);
}

function factorial(n) {
  if (n === 0) {return BigInt(1)}
  else {return BigInt(n) * factorial(n-1)}
}

console.log(sumFactorialDigits(25));