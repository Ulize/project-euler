// O(n^2) but complete
function specialPythagoreanTriplet(n) {
  for (let a = 1; a < n; a++) {
    for (let b = a+1; b < n; b++) {
      // a + b + c = n
      let c = n - a - b;
      // Check if it is a pythagorean triplet
      if (a*a + b*b == c*c) {
        return (a*b*c);
      }
    }  
  }
  return -1;
}

// Super fast but not complete since
// Euclid's formula without k factor
// can't generate all pythagorean triples
// Note: could add k parameter for completeness
function specialPythagoreanTriplet(x) {
  let m = 1;
  while (true) {
    m++;
    // a + b + c = x
    // mn + (m^2-n^2)/2 + (m^2+n^2)/2 = x
    // mn + m^2 = x
    // mn = x - m^2 
    // n = (x - m^2)/m 
    let n = (x - m**2) / m;
    if (n < 0) break;
    if (n > m) continue;
    // By Euclid's variant formula
    let a = m * n;
    let b = (m ** 2 - n ** 2) / 2;
    if (!Number.isInteger(b)) continue;
    let c = (m ** 2 + n ** 2) / 2;
    if (!Number.isInteger(c)) continue;
    return a * b * c;
  }
  return -1;
}

// Reference https://en.wikipedia.org/wiki/Pythagorean_triple#Generating_a_triple

// chatGPT solution adding the k factor
function trio_pitagorico(n) {
  for (let m = 2; m <= Math.floor(Math.sqrt(n)); m++) {
    for (let n = 1; n < m; n++) {
      if ((m - n) % 2 === 1 && gcd(m, n) === 1) {
        let k = 1;
        while (k * (2 * m ** 2 + 2 * m * n) <= n) {
          const a = k * (m ** 2 - n ** 2);
          const b = k * (2 * m * n);
          const c = k * (m ** 2 + n ** 2);
          if (a + b + c === n) {
            return a * b * c;
          }
          k++;
        }
      }
    }
  }
  return -1;
}

function gcd(a, b) {
  if (b === 0) {
    return a;
  }
  return gcd(b, a % b);
}
