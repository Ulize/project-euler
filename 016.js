function powerDigitSum(exponent) {
  return BigInt(2**exponent).toString().split('').reduce((sum, digit) => sum + parseInt(digit), 0);
}

console.log(powerDigitSum(128));