function multiplesOf3and5(number) {
  // Number of multiples of 3 below number
  let multiplesOf3 = Math.floor((number-1)/3);
  // Sum of the first "multiplesOf3" numbers, then multiplied 
  // by 3 to get the sum of the multiples of 3
  let sum3 = 3*(multiplesOf3 * (multiplesOf3 + 1)) / 2;

  // Number of multiples of 5 below number
  let multiplesOf5 = Math.floor((number-1)/5);
  // Sum of multiples of 5 below number
  let sum5 = 5*(multiplesOf5 * (multiplesOf5 + 1)) / 2;
  
  // Number of multiples of 15 below number
  let multiplesOf15 = Math.floor((number-1)/15);
  // Sum of multiples of 15 below number
  let sum15 = 15*(multiplesOf15 * (multiplesOf15 + 1)) / 2;

  // Since the multiples of 3 and multiples of 5
  // intersect on the multiples of 15, they're
  // added twice in the sum, so we substract them once
  return sum3 + sum5 - sum15;
}

// Reference https://en.wikipedia.org/wiki/Triangular_number#Formula