function largestPrimeFactor(number) {
  // We decompress the number on its prime factors
  let possibleFactor = 2;
  // When possibleFactor equals number we have the
  // largest prime factor
  while (possibleFactor != number) {
    // If it's actually a factor
    if (number % possibleFactor == 0) {
      number /= possibleFactor;
    }
    else {
      possibleFactor++;
    }
  }
  return number;
}

// Reference https://en.wikipedia.org/wiki/Integer_factorization#Prime_decomposition