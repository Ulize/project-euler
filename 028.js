function spiralDiagonals(n) {
  if (n == 1) {
    return 1;
  }
  else {
    let count = 4 * (n-2)**2 + (n-1)*(10);
    return spiralDiagonals(n-2) + count;
  }
}

spiralDiagonals(1001);