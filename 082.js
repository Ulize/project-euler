function pathSumThreeWays(matrix) {
  function minNeighbour(idx, arr, j) {
    let neighbours = [];
    neighbours.push([matrixCopy[idx][j-1], idx]);
    if (idx > 0) {
      neighbours.push([matrixCopy[idx-1][j], idx-1]);
    }
    if (idx < len - 1) {
      neighbours.push([matrixCopy[idx+1][j], idx+1]);
    }
    let min = neighbours[0][0];
    for (let i = 1; i < neighbours.length; i++) {
      min = Math.min(min, neighbours[i][0]);
    }
    if (min == neighbours[0][0]) {
      return min;
    }
    for (let i = 1; i < neighbours.length; i++) {
      if (neighbours[i][0] == min && arr.indexOf(neighbours[i][1]) == -1) {
        return min;
      }
    }
    return undefined;
  }
  function incrementMinimum(j, arr) {
    let min = [arr[0], matrixCopy[arr[0]][j]];
    for (let i of arr) {
      if (matrixCopy[i][j] < min[1]) {
        min = [i, matrixCopy[i][j]];
      }
    }
    return min;
  }
  let matrixCopy = matrix.map(rows => rows.slice());
  let len = matrixCopy.length;
  for (let j = 1; j < len; j++) {
    let arr = [...Array(len).keys()];
    while (arr.length > 0) {
      let lastMin = incrementMinimum(j-1, arr);
      matrixCopy[lastMin[0]][j] += lastMin[1];
      arr = arr.filter(elem => elem != lastMin[0]);
      for (let idx of arr) {
        let min = minNeighbour(idx, arr, j);
        if (min != undefined) {
          matrixCopy[idx][j] += min;
          arr = arr.filter(elem => elem != idx);
        }
      }
    }
  }
  let min = matrixCopy[0][len-1];
  for (let i = 1; i < len; i++) {
    min = Math.min(min, matrixCopy[i][len - 1]);
  }
  console.log(min);
  return min;
}

// Only change code above this line

const testMatrix1 = [
  [131, 673, 234, 103, 18],
  [201, 96, 342, 965, 150],
  [630, 803, 746, 422, 111],
  [537, 699, 497, 121, 956],
  [805, 732, 524, 37, 331]
];

console.log(pathSumThreeWays(testMatrix1));
