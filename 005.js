function smallestMult(n) {
  let res = 1
  for (let i = 2; i <= n; i++) {
    if (res % i != 0) {
      let mult = i
      for (let j = i - 1; j > 1; j--) {
        if (i % j == 0) {
          mult = i / j;
          break;
        }
      }
      res *= mult
    }
  }
  return res;
}