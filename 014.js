function longestCollatzSequence(limit) {
  let longest = 1;
  let longestLength = 1;
  for (let i = 2; i < limit; i++) {
    let length = 1;
    let n = i;
    while (n != 1) {
      if (n % 2 == 0) {
        n = n / 2;
      }
      else {
        n = 3*n + 1;
      }
      length++;
    }
    if (length > longestLength) {
      longest = i;
      longestLength = length;
    }
  }
  return longest;
}

longestCollatzSequence(14);