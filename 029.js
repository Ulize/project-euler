function distinctPowers(n) {
  let arr = {};
  let count = 0;
  for (let a = 2; a <= n; a++) {
    for (let b = 2; b <= n; b++) {
      if (!arr[a**b]) {
        arr[a**b] = 1;
        count++; 
      }
    }
  }
  return count;
}

distinctPowers(30);