let subcases = {}

function coinSums(n) {
  if (subcases[n]) {
    return subcases[n];
  }
  let total = 1;
  if (n > 200) {
    total = coinSums(n-200) + coinSums(200);
  }
  else if (n == 200) {
    total = coinSums(n-100) + coinSums(100);
  }
  else if (n > 100) {
    total = coinSums(n-100) + coinSums(100);
  }
  else if (n > 50) {
    total = coinSums(n-50) + coinSums(50);
  }
  else if (n > 20) {
    total = coinSums(n-20) + coinSums(20);
  }
  else if (n > 10) {
    total = coinSums(n-10) + coinSums(10);
  }
  else if (n > 5) {
    total = coinSums(n-5) + coinSums(5);
  }
  else if (n > 2) {
    total = coinSums(n-2) + coinSums(2);
  }
  else if (n > 1) {
    total = n;
  }
  subcases[n] = total;
  return total;
}

console.log(coinSums(12));