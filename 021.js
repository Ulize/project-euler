
function divisorsSum(n, arr) {
  if (arr[n]) {return arr[n];}
  let count = 1;
  for (let i = 2; i <= n / 2; i++) {
    if (n % i == 0) {
      count += i;
    }
  }
  arr[n] = count;
  return count;
}

function sumAmicableNum(n) {
  let storedSums = Array(n).fill(0);
  let count = 0;
  for (let a = 2; a < n; a++) {
    let b = divisorsSum(a, storedSums);
    if (a === divisorsSum(b, storedSums) && a !== b) {
      count += a;
    }
  }
  return count;
}
console.log(sumAmicableNum(300));