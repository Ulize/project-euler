function largeSum(arr) {
  let count = 0;
  for (let number in arr) {
    count += parseInt(arr[number]);
  }
  return 1e+9 * count.toString().substring(0,11);
}

// Only change code above this line

const testNums = [
  '37107287533902102798797998220837590246510135740250',
  '46376937677490009712648124896970078050417018260538'
];

largeSum(testNums);