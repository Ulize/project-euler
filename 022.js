let alphabet = ' abcdefghijklmnopqrstuvwxyz'.split('')

function nameScore(name, index) {
  let lettersScore = 0;
  for (let i in name) {
    lettersScore += alphabet.indexOf(name[i].toLowerCase());
  }
  return lettersScore * index;
}

function namesScores(arr) {
  return arr.slice()
            .sort()
            .reduce((sum, name, index) => sum + nameScore(name, index+1), 0);
}

// Only change code above this line
const test1 = ['THIS', 'IS', 'ONLY', 'A', 'TEST'];
const test2 = ['I', 'REPEAT', 'THIS', 'IS', 'ONLY', 'A', 'TEST'];

console.log(namesScores(test1));