function fiboEvenSum(n) {
  // F_(i-1)
  let previous = 1;
  // F_i
  let actual = 2;
  // Sum progress
  let sum = 0;

  while (actual <= n) {
    // If F_i is even
    if (actual % 2 == 0) {
      sum += actual;
    }
    // Update values
    [previous, actual] = [actual, previous + actual];
  }
  return sum;
}

// Reference https://en.wikipedia.org/wiki/Fibonacci_sequence#Definition