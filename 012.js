function divisibleTriangleNumber(n) {
  let i = 1;
  for (let j = 2; true; j++) {
    let divisors = 2;
    for (let k = 2; k < Math.sqrt(i); k++) {
      if (i % k == 0) {
        if (k * k != i) {
          divisors += 2;
        } 
        else {
          divisors += 1;
        }
      }
    }
    if (divisors > n) {
      return i;
    }
    i += j;
  }
}

console.log(divisibleTriangleNumber(23));
