function sumSquareDifference(n) {
  // Get sum of the squares of the first n natural numbers
  let sumSquares = (n * (n + 1) * (2 * n + 1)) / 6
  // Get square of the sum of the first n natural numbers
  let squareSum = ((n * (n + 1)) / 2) ** 2
  return squareSum - sumSquares;
}

sumSquareDifference(100);

// References 
// https://en.wikipedia.org/wiki/Square_pyramidal_number#Formula
// https://en.wikipedia.org/wiki/Triangular_number#Formula