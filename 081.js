function pathSumTwoWays(matrix) {
  let copyMatrix = matrix.map(row => row.slice());
  let len = copyMatrix.length;
  for (let i = 1; i < len; i++) {
    let array = []
    copyMatrix[i][0] += copyMatrix[i-1][0];
    copyMatrix[0][i] += copyMatrix[0][i-1];
    for (let j = 1; j < i; j++) {
      copyMatrix[i-j][j] += Math.min(copyMatrix[i-j-1][j], copyMatrix[i-j][j-1])
    }
  }
  for (let i = len; i < 2 * (len - 1) + 1; i++) {
    for (let j = i - len + 1; j < len; j++) {
      copyMatrix[i-j][j] += Math.min(copyMatrix[i-j-1][j], copyMatrix[i-j][j-1])
    }
  }
  return copyMatrix[len-1][len-1];
}

// Only change code above this line

const testMatrix1 = [
  [131, 673, 234, 103, 18],
  [201, 96, 342, 965, 150],
  [630, 803, 746, 422, 111],
  [537, 699, 497, 121, 956],
  [805, 732, 524, 37, 331]
];

pathSumTwoWays(testMatrix1);
