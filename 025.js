function digitFibonacci(n) {
  // F_0
  let previous = 0;
  // F_1
  let actual = 1;
  let i = 1;

  while (actual.toString().length < n) {
    // Update values
    [previous, actual] = [actual, previous + actual];
    i++;
  }
  return i;
}

digitFibonacci(2);